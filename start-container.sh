#!/bin/bash
DOCKER_IMAGE="jalien-dev"
docker run --privileged -d --name jalien -it --rm \
	-v $PWD/workdir:/workdir:Z -v $PWD/utils:/utils -v $HOME/alice/sw:/remote_store \
	"$DOCKER_IMAGE" bash
