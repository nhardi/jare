#!/bin/bash
set -e

PKGS="tmux htop build-essential python3-pip gfortran libncurses-dev curl rsync
      iputils-ping libcurl4-openssl-dev libxml2-dev zlib1g-dev git
      openjdk-11-jdk autoconf automake autopoint texinfo gettext vim
      libtool libtool-bin pkg-config dialog flex bison tree silversearcher-ag"

PIPKGS="alibuild"

apt update && apt upgrade -y && apt install $PKGS -y
pip3 install $PIPKGS
